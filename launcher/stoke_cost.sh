#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Wrong Input! Please run the script in this format:"
  echo "./$0 <synthesize_configuration_file>"
  exit 0
fi

input_file=$1

echo "Cost Analysis Started"
base_cmd="stoke debug cost "
binary="./a.out"
binary_arg="1000000"
test_file="./stoke_test.sh"
testing=1;
time_script="time.sh"

#Input for running stoke debug cost

#Target
target="--target "
target+="target.s"
base_cmd+="$target "

#Rewrite
rewrite="--rewrite "
#Will be set later in the loop over results_dir

#Def_in
#awk '$1 == "--def_in" {print }' synthesize.conf | awk -F# '{print $1}'
def_in_cmd="awk '"'$1 == "--def_in"'" {print }' $input_file | awk -F# '{print "'$1}'"'"
def_in=$(eval $def_in_cmd)
if [ "$def_in" == "" ]; then
  echo "Wrong Input!"
  exit 0
fi
base_cmd+=$def_in

#Live_out
#awk '$1 == "--live_out" {print }' synthesize.conf | awk -F# '{print $1}'
live_out_cmd="awk '"'$1 == "--live_out"'" {print }' $input_file | awk -F# '{print "'$1}'"'"
live_out=$(eval $live_out_cmd)
if [ "$live_out" == "" ]; then
  echo "Wrong Input!"
  exit 0
fi
base_cmd+=$live_out


#Array for different cost functions
costArray=("binsize" "size" "latency" "measured" "sseavx")


#File name for saving costs
file="cost.txt"
if [ -f $file ] ; then
  rm $file
fi


#Results
#awk '$1 == "--results" {print $2}' synthesize.conf
results_dir_cmd="awk '"'$1 == "--results"'" {print "'$2}'"' $input_file | awk -F# '{print "'$1}'"'"
results_dir=$(eval $results_dir_cmd)
if [ "$results_dir" == "" ]; then
  echo "Wrong Input!"
  exit 0
fi
#Changed to final result directory
results_dir="final_results/"

#Copy target.s into the results_dir
#cp target.s $results_dir
#file "0" in the final_results

results_dir="$results_dir*"

for filename in $results_dir; do

  final_cmd="$base_cmd $rewrite$filename "
  #echo $final_cmd

  #Run 'stoke debug cost' for every cost function by passing --cost to final_cmd
  echo "Testing $filename"
  echo "Name: $filename" >> $file
  for var in "${costArray[@]}"
  do
    echo "${var}"
    command="$final_cmd"'--cost "'${var}'"'" >> $file"
    #echo "$command"
    eval $command
  done



  #Replace the current rewrite in the binary
  echo "Replace rewrite into Binary"
  replace_cmd="stoke replace -i $binary"
  new_replace="$replace_cmd --rewrite $filename"
  echo $new_replace
  eval $new_replace

  #Check Execution time
  echo "Measure execution time"

  #Get time by ./a.out or test.sh

  #with test file: test.sh

  #Getting time with "time" command
  #time_cmd="(time $test_file) > /dev/null 2>time.log"

  #Getting time with time script: time.sh
  #time_cmd="$time_script $test_file"

  #time_script selected, no need to check binary_arg -> arg not defined in time script
  #if [ $testing -ne 1 ];then
  #  #With direct arg
  time_cmd="$time_script $binary $binary_arg"

  # exit 0
  #fi
  #eval $time_cmd
  ##awk '{printf "%s ",$0} END {print ""}' time.log
  #time_cmd_awk="awk '{printf "'"%s ",$0} END {print ""}'"' time.log"
  #time_result=$(eval $time_cmd_awk)


  time_result=$(eval $time_cmd)

  echo "Time: $time_result" >> $file


  #Check Cache misses

  #Valgrind

  echo "Valgrind measurements - Cachegrind"
  #Valgrind command for cache measurements
  valgrind_cache_cmd="valgrind --tool=cachegrind $binary $binary_arg > /dev/null 2>valgrind.log"
  eval $valgrind_cache_cmd

  #To get info from the function misses: Ingore for now
  #cg_annotate cachegrind.out.4384 > valgrind.log
  #cat valgrind.log | grep pop


  #awk '$2$3=="I1misses:" {print $4}' valgrind.log
  awk_valgrind_cmd="awk '"'$2$3=="I1misses:" {print $4}'"' valgrind.log"
  I1misses=$(eval $awk_valgrind_cmd)
  echo "I1misses: $I1misses" >> $file

  #awk '$2$3=="D1misses:" {print $4}' valgrind.log
  awk_valgrind_cmd="awk '"'$2$3=="D1misses:" {print $4}'"' valgrind.log"
  D1misses=$(eval $awk_valgrind_cmd)
  echo "D1misses: $D1misses" >> $file

  #awk '$2$3=="LLimisses:" {print $4}' valgrind.log
  awk_valgrind_cmd="awk '"'$2$3=="LLimisses:" {print $4}'"' valgrind.log"
  LLimisses=$(eval $awk_valgrind_cmd)
  echo "LLimisses: $LLimisses" >> $file

  #awk '$2$3=="LLdmisses:" {print $4}' valgrind.log
  awk_valgrind_cmd="awk '"'$2$3=="LLdmisses:" {print $4}'"' valgrind.log"
  LLdmisses=$(eval $awk_valgrind_cmd)
  echo "LLdmisses: $LLdmisses" >> $file


  #Perf

  echo "Perf measurements"

  #Perf command for cache measurements
  perf_cache_cmd="sudo perf stat -r 10 -x, -e cycles,L1-dcache-loads,L1-dcache-load-misses,L1-dcache-stores,L1-icache-load-misses,dTLB-loads,dTLB-load-misses,dTLB-stores,dTLB-store-misses,LLC-loads,LLC-stores,cache-references,cache-misses,mem-loads,mem-stores $binary $binary_arg > /dev/null 2>perf.log"
  echo $perf_cache_cmd
  eval $perf_cache_cmd
  #Not sure if we need to flush cache before running perf measurements


  #awk -F, '$3 == "cycles" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "cycles" {print $1}'"' perf.log"
  cycles=$(eval $awk_perf_cmd)
  echo "Cycles: $cycles" >> $file

  #awk -F, '$3 == "L1-dcache-loads" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "L1-dcache-loads" {print $1}'"' perf.log"
  L1_dcache_loads=$(eval $awk_perf_cmd)
  echo "L1-dcache-loads: $L1_dcache_loads" >> $file

  #awk -F, '$3 == "L1-dcache-load-misses" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "L1-dcache-load-misses" {print $1}'"' perf.log"
  L1_dcache_load_misses=$(eval $awk_perf_cmd)
  echo "L1-dcache-load-misses: $L1_dcache_load_misses" >> $file

  #awk -F, '$3 == "L1-dcache-stores" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "L1-dcache-stores" {print $1}'"' perf.log"
  L1_dcache_stores=$(eval $awk_perf_cmd)
  echo "L1-dcache-stores: $L1_dcache_stores" >> $file

  #awk -F, '$3 == "L1-icache-load-misses" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "L1-icache-load-misses" {print $1}'"' perf.log"
  L1_icache_load_misses=$(eval $awk_perf_cmd)
  echo "L1-icache-load-misses: $L1_icache_load_misses" >> $file

  #awk -F, '$3 == "dTLB-loads" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "dTLB-loads" {print $1}'"' perf.log"
  dTLB_loads=$(eval $awk_perf_cmd)
  echo "dTLB-loads: $dTLB_loads" >> $file

  #awk -F, '$3 == "dTLB-load-misses" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "dTLB-load-misses" {print $1}'"' perf.log"
  dTLB_load_misses=$(eval $awk_perf_cmd)
  echo "dTLB-load-misses: $dTLB_load_misses" >> $file

  #awk -F, '$3 == "dTLB-stores" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "dTLB-stores" {print $1}'"' perf.log"
  dTLB_stores=$(eval $awk_perf_cmd)
  echo "dTLB-stores: $dTLB_stores" >> $file

  #awk -F, '$3 == "dTLB-store-misses" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "dTLB-store-misses" {print $1}'"' perf.log"
  dTLB_store_misses=$(eval $awk_perf_cmd)
  echo "dTLB_store_misses: $dTLB_store_misses" >> $file

  #awk -F, '$3 == "LLC-loads" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "LLC-loads" {print $1}'"' perf.log"
  LLC_loads=$(eval $awk_perf_cmd)
  echo "LLC-loads: $LLC_loads" >> $file

  #awk -F, '$3 == "LLC-stores" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "LLC-stores" {print $1}'"' perf.log"
  LLC_stores=$(eval $awk_perf_cmd)
  echo "LLC_stores: $LLC_stores" >> $file

  #awk -F, '$3 == "cache-references" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "cache-references" {print $1}'"' perf.log"
  cache_references=$(eval $awk_perf_cmd)
  echo "cache-references: $cache_references" >> $file

  #awk -F, '$3 == "cache-misses" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "cache-misses" {print $1}'"' perf.log"
  cache_misses=$(eval $awk_perf_cmd)
  echo "cache-misses: $cache_misses" >> $file

  #awk -F, '$3 == "mem-loads" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "mem-loads" {print $1}'"' perf.log"
  mem_loads=$(eval $awk_perf_cmd)
  echo "mem-loads: $mem_loads" >> $file

  #awk -F, '$3 == "mem-stores" {print $1}' perf.log
  awk_perf_cmd="awk -F, '"'$3 == "mem-stores" {print $1}'"' perf.log"
  mem_stores=$(eval $awk_perf_cmd)
  echo "mem-stores: $mem_stores" >> $file

done

new_file="new_$file"

#awk -F: '{print $1 $2}' cost.txt > new_cost.txt
awk_cost_format="awk -F: '"'{print $1 $2}'"' $file >> $new_file"
eval $awk_cost_format

echo "Cost Analysis Finished -> Results in: $file and $new_file"



