#!/usr/bin/python
# -*- coding: utf-8 -*-

###############################################################################################
###        Name: stoke_launcher.py
###      Author: Georgios Katsikas - georgios.katsikas@ri.se
### Description: Automated deployment of Stoke experiments.
###############################################################################################

import os
import shutil
import fnmatch
import argparse
import joblib

from subprocess import Popen, PIPE

"""
python stoke_launcher.py --input-app ../examples/ --iterations 5
"""

SUCCESS = True
FAILURE = False
ERROR   = -1
MAX_SUBPROC_RETRY = 5

APPLICATIONS  = []
EXCLUDED_APPS = ["nacl_shootout", "tutorial", "strata", "bansal"]
STOKE_SEARCH  = os.path.abspath("./stoke_search.sh")
STOKE_COST    = os.path.abspath("./stoke_cost.sh")

def get_project_pwd():
    return os.getcwd()

def execute_command(command):
    # Execute command
    p = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
    out, err = p.communicate()

    # Get status
    p_status = p.wait()
    if (p_status != 0) or ("error" in err):
        return [FAILURE, out]

    return [SUCCESS, out]

def get_command(executable, args="", background="&", project_path=False, to_file=None):
    # Redirect both stderr and stdout to /dev/null
    if background:
        dest = to_file if to_file else "/dev/null"
        actual_cmd = "{} {} &>{}".format(executable, args, dest)
    else:
        actual_cmd = "{} {}".format(executable, args)

    if   project_path == True:
        pr_path = get_project_pwd()
    elif project_path == False:
        pr_path = ""
    else:
        pr_path = project_path

    pr_cd_path = ""
    if pr_path:
        pr_cd_path = "cd {}; ".format(pr_path)

    return "bash -c \"{project_path} {cmd} {bg}\"".format(\
        project_path = pr_cd_path,
        cmd          = actual_cmd,
        bg           = background\
    )

def get_proc_list():
    proc_list = []

    cmd  = "ps aux"
    [status, out] = execute_cmd(cmd)

    if not status:
        return proc_list

    sub_proc = out

    #Discard the first line (ps aux header)
    sub_proc = sub_proc.splitlines()[1:]

    for line in sub_proc:
        #The separator for splitting is 'variable number of spaces'
        proc_info = split(" *", line)
        proc_list.append(proc_info)

    return proc_list

def get_proc_list_filter(proc_filter, all_procs=False, exclude_pattern=False):
    proc_list = []

    for line in get_proc_list():
        # Filter what we want to keep
        if  any(proc_filter in s for s in line) and \
        not any("grep" in s for s in line) and \
        (not exclude_pattern or not any(exclude_pattern in s for s in line)):
            if all_procs:
                proc_list.append(line)
            else:
                proc_list = line
                break

    return proc_list

def pid_exists(pid):
    res = get_proc_list_filter(str(pid))
    if not res:
        return FAILURE
    return SUCCESS

def kill_process_pids(pids_list, force=False, verbose=False):
    kill_arg = "SIGTERM" if force else "SIGINT"

    for pid in pids_list:
        if (not pid) or (pid <= 0):
            status = FAILURE
            continue

        [status, _] = execute_cmd("kill -{} {}".format(kill_arg, pid))

        if status:
            if verbose:
                print("|-->\tProcess {} killed on {}".format(pid, machine_ip))

    return status

def retrieve_pid(process_name, all_procs=False, exclude_pattern=False, verbose=False):
    """
    Look for a specific (set of) process name(s) in the OS of machine_ip.
    Take a few chances or get out of there with a (list of) PID(s).
    Returns a list of PID(s) or an empty list.
    """

    i     = 0
    found = False

    while i <= MAX_SUBPROC_RETRY:
        # Retrieve a (filtered) list of processes' information
        res = get_proc_list_filter(process_name, all_procs, exclude_pattern)
        if verbose:
            print res

        bad_pid = []

        # Empty list means that this process is not present in the system
        if not res:
            if verbose:
                print("|-->\tUnable to fetch PID for {} from {}. Retrying...".\
                            format(process_name, machine_ip))
        else:
            # Check if process is in un-interruptible sleep state
            # If so, it is likely that it will not react upon any signal,
            # so it is better to exit with error and let the control loop
            # to re-execute the process
            state   = [p[7] for p in res] if all_procs else [res[7]]
            if any(["D" in s for s in state]):
                bad_pid = [p[1] for p in res] if all_procs else [res[1]]
                print("|-->\tProcess {} in un-interruptible state D"\
                    .format(process_name))
                break


            # Now we are kind of sure that the process is there
            found = True
            break
        i += 1

    # Check one more time
    pid = [int(p[1]) for p in res] if found else [] \
        if all_procs \
        else [int(res[1])] if found else []
    if verbose:
        print "Retrieved PID {}".format(pid)

    if bad_pid:
        if verbose:
            print "Killing {}".format(bad_pid)
        kill_process_pids(bad_pid, force=True, verbose=verbose)

    if (not pid) or (not pid_exists(pid[0])):
        return []

    return pid

def find_makefile_folders(path, pattern):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.abspath(root))
                # result.append(os.path.join(root, name))
    return result

def folder_exists(input_folder, verbose=False):
    if not os.path.isdir(input_folder):
        raise RuntimeError("Applications folder {} does not exist".format(input_folder))
    if verbose:
        print("Found applications folder: {}\n".format(input_folder))

def list_applications(apps_folder, verbose=False):
    applications = []

    print("Available applications:")
    for folder in os.listdir(apps_folder):
        app = os.path.abspath(os.path.join(apps_folder, folder))

        # Some of the applications do not follow the "regular" pipeline
        if folder in EXCLUDED_APPS:
            continue

        applications.append(app)

        if verbose:
            print("{}".format(app))

    if verbose:
        print("")

    return applications

def check_and_set_environment():
    print("Setting environment variable PATH:")

    if "PATH" not in os.environ:
        raise RuntimeError("Environment variable PATH does not exist")

    if "stoke/bin" not in os.environ["PATH"]:
        os.environ["PATH"] = os.environ["PATH"] + ":{}".format(get_project_pwd())
    else:
        return

    if "stoke/bin" not in os.environ["PATH"]:
        raise RuntimeError("Environment variable PATH does not contain STOKE")

    print("{}\n".format(os.environ["PATH"]))


def run_applications(apps_folder, iterations, verbose=False):
    all_folders = []
    for app in apps_folder:
        makefile_dirs = find_makefile_folders(app, "Makefile")
        all_folders += makefile_dirs

        # run_application(app, iterations, verbose)

    print all_folders

    REMAINING = []
    # completed = ["p20", "p16", "p24", "p08", "p01", "p14", "p09", "p13", "p12", "p23", "p22",
    # "p03", "p02", "p15", "p21", "p07", "p25", "p06", "p05" ]

    completed = ["exp", "p20", "parity", "p12", "p13",  "p09", "p14", "p01",
    "saxpy", "p08", "p24", "p16", "p07", "p21", "p25", "p15", "p03", "p22",
    "p23","p06", "p05", "p19", "p11", "p17","p02", "p04", "montmul", "p18", "exp"]

    for fldr in all_folders:
        skip = False
        for d in completed:
            if d in fldr:
                skip = True
        if not skip:
            REMAINING.append(fldr)
    print REMAINING


    tasks = (joblib.delayed(run_application)
        (app_folder, iterations, verbose)
        for app_folder in REMAINING
        )
    res = joblib.Parallel(n_jobs=1, verbose=50)(tasks)

def run_application(app_folder, iterations, verbose=False):
    print("Superoptimizing application: {}".format(app_folder))


    # # Clean up this application
    # clean_up(app_folder, verbose)

    # # Run all steps before the first synthesis
    # run_preparation(app_folder, verbose)

    # # Synthesize and optimize
    # run_synthesis_and_opt(app_folder, iterations, verbose)

        # Apply cost function and measure
    run_cost_and_measure(app_folder, verbose=True)

        # TODO: Remove when code becomes stable (if it ever does :p)
        # break

    return

def run_preparation(makefile_dir, verbose=False):
    print("    #####################################################################")
    print("    ### PREPARE")
    print("    #####################################################################")
    # Preparation command
    cmd = get_command(
        executable   = "make orig_gcc && make extract && make testcase",
        args         = "",
        background   = "&",
        project_path = makefile_dir
    )
    if verbose:
        print("    Preparation command: {}".format(cmd))

    # Fire and wait
    [status, out] = execute_command(cmd)
    if not status:
        raise RuntimeError("Failed to execute preparation: {}".format(cmd))

    print("    Successful preparation!")
    print("    #####################################################################")
    print("")

def run_synthesis_and_opt(makefile_dir, iterations, verbose=False):
    print("    #####################################################################")
    print("    ### SYNTHESIZE + SEARCH")
    print("    #####################################################################")
    for i in range(iterations):
        # Initial synthesis command
        cmd = get_command(
            executable   = "make search_synth",
            args         = "",
            background   = "",
            project_path = makefile_dir
        )
        print("    [Iteration {}]    Synthesis command: {}".format(i, cmd))

        # Fire and wait
        [status, out] = execute_command(cmd)
        if not status:
            print "Failed to execute synthesis: {}".format(cmd)

        #######################################################################

        # STOKE Search command
        cmd = get_command(
            executable   = "{} opt.conf {}".format(STOKE_SEARCH, iterations),
            args         = "",
            background   = "",
            project_path = makefile_dir
        )
        print("    [Iteration {}] STOKE Search command: {}".format(i, cmd))

        # Fire and wait
        [status, out] = execute_command(cmd)
        if not status:
            print "Failed to execute STOKE Search: {}".format(cmd)

    print("    #####################################################################")
    print("")

def run_cost_and_measure(makefile_dir, verbose=False):
    print("    #####################################################################")
    print("    ### MEASURE COST")
    print("    #####################################################################")
    for i in range(iterations):
        # STOKE Cost command
        cmd = get_command(
            executable   = "{} opt.conf".format(STOKE_COST),
            args         = "",
            background   = "",
            project_path = makefile_dir
        )
        print("    [Iteration {}] STOKE   Cost command: {}".format(i, cmd))

        # Fire and wait
        [status, out] = execute_command(cmd)
        if not status:
            raise RuntimeError("Failed to execute STOKE Cost: {}".format(cmd))
    print("    #####################################################################")
    print("")

def clean_up(makefile_dir, verbose=False):
    synth_file = os.path.join(makefile_dir, "synth_result.s")
    if os.path.isfile(synth_file):
        os.remove(synth_file)

    res_dir = os.path.join(makefile_dir, "results")
    if os.path.isdir(res_dir):
        shutil.rmtree(res_dir)

    res_dir = os.path.join(makefile_dir, "final_results")
    if os.path.isdir(res_dir):
        shutil.rmtree(res_dir)

if __name__ == "__main__":

    parser = argparse.ArgumentParser("Mantis Predictor")
    parser.add_argument(
        "--input-apps",
        type=str,
        default="../examples/",
        help="Input folder where applications are located"
    )
    parser.add_argument(
        "--iterations",
        type=int,
        default=2,
        help="Number of iterations per experiment"
    )
    parser.add_argument(
        "--verbose",
        action="store_true",
        default=True,
        help=""
    )

    arguments = parser.parse_args()

    verbose = arguments.verbose

    input_apps_folder = os.path.abspath(arguments.input_apps)
    folder_exists(input_apps_folder, verbose)

    iterations = arguments.iterations
    if iterations <= 0:
        raise RuntimeError("Specify a valid number of iterations per experiment")

    check_and_set_environment()

    APPLICATIONS = list_applications(input_apps_folder, verbose)

    run_applications(APPLICATIONS, 1, verbose)
