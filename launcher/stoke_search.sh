#!/bin/bash

echo "Search Process Started"

# Check if there is an input
if [ "$#" -ne 2 ]; then
	echo "Wrong Input! Please run the script in this format:"
	echo "./$0 <synthesize_configuration_file> <iterations>"
	exit 1
fi

input_file=$1
iterations=${2:-2}

if [[ ! -f $input_file ]]; then
	echo "Invalid synthesized configuration file: "$input_file
	exit 1
fi

if [[ $iterations -lt "0" ]]; then
	echo "Please provide a positive number of iterations"
	exit 1
fi

new_conf="new.conf"

# Check here for Time.sh
check_perf_cmd="cat /proc/sys/kernel/perf_event_paranoid"
check_perf_val=$(eval $check_perf_cmd)
if [ "$check_perf_val" != "-1" ]; then
  echo "Run these first:"
  echo "sudo sh -c 'echo -1 >/proc/sys/kernel/perf_event_paranoid'"
  echo "sudo sh -c 'echo 0 > /proc/sys/kernel/nmi_watchdog'"
  exit 1
fi

# Get --results from .conf file
results_dir_cmd="awk '"'$1 == "--results"'" {print "'$2}'"' $input_file | awk -F# '{print "'$1}'"'"
results_dir=$(eval $results_dir_cmd)
if [ "$results_dir" == "" ]; then
	echo "Wrong Input!"
	exit 1
fi

# Create folder just in case it hasn't been created yet
mkdir $results_dir -f

final_results_dir="final_results/"
results_dir_for="$results_dir*"


cmp_cmd_base="cmp --silent "
final_results_dir_for="$final_results_dir*"
#search_cmd="stoke synthesize --config $new_conf"
search_cmd="stoke search --config $new_conf"



# Check if output directory exists
if [ ! -d "$final_results_dir" ]; then
	mkdir $final_results_dir
fi


# Search

# Array for different cost functions
costArray=("binsize" "size" "latency" "measured")
base_cost_function="correctness + "

for var in "${costArray[@]}"
do
	echo "${var}"
  	# Change the cost function and create a new conf file
  	#awk '$1 == "--cost" {print}' synthesize.conf | awk -F'"' '{print $2}'
  	cost_function_cmd="awk '"'$1 == "--cost"'" {print }' $input_file | awk -F'"'"'"' '{print "'$2}'"'"
  	current_cost_function=$(eval $cost_function_cmd)
  	#echo "current cost= $current_cost_function"

  	new_cost_function="$base_cost_function${var}"
  	echo "Testing for cost= $new_cost_function"

  	if [ -f $new_conf ] ; then
  		rm $new_conf
	fi

  	change_cost_cmd="sed 's/$current_cost_function/$new_cost_function/' $input_file > $new_conf"
  	eval $change_cost_cmd


	# Run search several times for each cost function
	for i in {1..$iterations..1}
	do

		# Remove results directory
		# Check if results directory exists
		if [ -d "$results_dir" ]; then
			rm -r $results_dir
		fi

		# Run Stoke search
      echo $search_cmd
		eval $search_cmd


		# Check if there is any file in the output directory
		# If not copy target.s in it
		new_number="0"
		ls_cmd="ls $final_results_dir"
		ls_output=$(eval $ls_cmd)

		if [ "$ls_output" == "" ]; then
			cp target.s "$final_results_dir$new_number"
		fi


		# Copy the different sources into the output directory
		for inputfile in $results_dir_for; do
			if_same=0
			for outputfile in $final_results_dir_for; do
				#echo "$inputfile $outputfile"
				cmp_cmd="$cmp_cmd_base$inputfile $outputfile || echo "'"files are different"'
				echo "$cmp_cmd"
				cmp_output=$(eval $cmp_cmd)
				echo "$cmp_output"
				if [ "$cmp_output" != "files are different" ]; then
					if_same=1
					echo "if same = yes"
				fi
			done

			if [ $if_same == "0" ]; then
				echo "find no similar"
				# Find the number of last file in the output directory
				ls_tail_cmd="ls -v $final_results_dir | tail -n1"
				last_number=$(eval $ls_tail_cmd)
				new_number="$(($last_number + 1))"
				#echo "$new_number"
				cp $inputfile "$final_results_dir$new_number"
			fi
		done
	done
done

