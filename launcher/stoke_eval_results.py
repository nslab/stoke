#!/usr/bin/python
import os
import csv
import argparse
import re
import pandas as pd
import numpy as np
import math
from scipy import stats

NAME_COLUMN = 'Name'

def get_tokens_from_line(line):
    tokens = line.split(":")
    assert(len(tokens) == 2)
    tokens = [x.lstrip().rstrip() for x in tokens]

    return tokens

def integrate_tokens2row_dic(tokens, row_dic):
    assert(len(tokens) == 2)

    """ It is possible that value tokens comprised of a multiple comma separated values.
    e.g.,: (D1misses: 6,598) in that case we remove comma and convert it into a single value
    e.g.,: 6598"""
    tokens[1] = re.sub(',', '', tokens[1])

    subtokens = tokens[1].split(",")

    row_dic[tokens[0]] = tokens[1]

    return row_dic


def result_costs_file_2_dictionary(ifile):
    """ Parsing file of the format:

    Name: final_results/0
    binsize: 25
    size: 12
    latency: 8
    measured: 8
    sseavx: 0
    Time: 0.002800009
    I1misses: 2,806
    D1misses: 6,598
    LLimisses: 2,352
    LLdmisses: 4,551
    Cycles: 2406975
    ...
    Each Name indicate a new ROW
    """

    exp_rows = []
    with open(ifile) as f:
        content = f.readlines()

        row_dic = {}
        for line in content:

            tokens = get_tokens_from_line(line)
            # row_dic = integrate_tokens2row_dic(tokens, row_dic)

            if tokens[0] == NAME_COLUMN:
                # This is start of the new row

                if (len(row_dic) > 0):
                    # We are reading the first row
                    exp_rows.append(row_dic)
                    row_dic = {}

                row_dic[tokens[0]] = tokens[1]

            else: # Any other row
                row_dic = integrate_tokens2row_dic(tokens, row_dic)

        # print columns
        # print row_dic
        exp_rows.append(row_dic) # Add last row
    return exp_rows

def res_dic_2_csv(res_dic_list, ofile_csv, delimeter=",", col_index=0):

    first_row = res_dic_list[col_index]
    all_columns = first_row.keys()
    all_columns.remove(NAME_COLUMN)
    all_columns = sorted(all_columns)


    # all_columns = first_row.keys().remove(NAME_COLUMN)
    print all_columns
    all_columns = [NAME_COLUMN] + all_columns

    print all_columns

    with open(ofile_csv, "wb") as output_file:
        dict_writer = csv.DictWriter(output_file, all_columns, delimiter=delimeter)
        dict_writer.writeheader()
        dict_writer.writerows(res_dic_list)


def get_smallest_execution_time_index(df):
    print df["Time"].argmin()
    print df["Time"].argmax()


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def estimate_percentile_of_a_row_in_a_column(df, target_column, target_row_index):
    """ We have a dataframe as an input, column name and an index. Using this index
    we select a value in the target column, now we want to estimate the percentile of this
    value in the target column.
    """

    value = df.loc[target_row_index][target_column]
    if is_number(value):

        value = np.float(float(value))
    else:
        print "ERROR, can not convert value to float: ", value


    all_values = df[target_column]
    res_values = []

    for x in all_values:
        if is_number(x) and not math.isnan(float(x)):
        # if is_number(x):
            res_values.append(np.float(x))

    # res_values = res_values[~np.isnan(res_values)]
    np_all_values = np.asarray(res_values)
    # np_all_values = np_all_values[~np.isnan(np_all_values)]


    percentile = stats.percentileofscore(np_all_values, value, kind='strict')

    if target_column == "latency":
        print "+++", value, "+++", percentile, "+++"
        print np_all_values
        print "+++"
        print np.percentile(np_all_values, 99, axis=0)
        print "ppp"




    print target_column, value, percentile

    return percentile





def estimate_ranks_by_execution_time(res_dic_list):
    df = pd.DataFrame.from_dict(res_dic_list)


    # print df
    # print df.columns
    # print df["Time"]

    min_index = df["Time"].argmin()
    max_index = df["Time"].argmax()
    # col = "mem-loads"
    #
    print "MIN MAX TIMES", min_index, max_index
    # ltime =  list(df["Time"])
    # llatency = list(df["latency"])
    # print llatency[max_index]
    # print llatency[min_index]
    # exit(0)

    min_dic = {}
    for col in df.columns:
        p = estimate_percentile_of_a_row_in_a_column(df, col, min_index)
        min_dic[col] = p
        if col == "latency":
            print "min percentile ", p

    max_dic = {}
    for col in df.columns:
        p = estimate_percentile_of_a_row_in_a_column(df, col, max_index)
        max_dic[col] = p

        if col == "latency":
            print list(df[col])
            print "max percentile ", p



    min_dic["A_sample_count"] = len(df)
    max_dic["A_sample_count"] = len(df)

    return min_dic, max_dic



def process_stoke_result_costs_file(ifile, ofile_csv):

    res_dic_list = result_costs_file_2_dictionary(ifile)

    min_td, max_td = estimate_ranks_by_execution_time(res_dic_list)
    # print "MIN MAX INDEXES", min_td, max_td

    res_dic_2_csv(res_dic_list, ofile_csv)


    return min_td, max_td



def plot_relevant_columns(icsv_file="res.csv", prefix="test"):

    column_pairs = []
    column_pairs.append(("Time", "Cycles"))
    column_pairs.append(("Time", "D1misses"))


    column_pairs.append(("Time", "I1misses"))
    column_pairs.append(("Time", "L1-dcache-load-misses"))
    column_pairs.append(("Time", "L1-dcache-loads"))
    column_pairs.append(("Time", "L1-dcache-stores"))

    column_pairs.append(("Time", "L1-icache-load-misses"))
    column_pairs.append(("Time", "LLC-loads"))
    column_pairs.append(("Time", "LLC_stores"))
    column_pairs.append(("Time", "LLdmisses"))
    column_pairs.append(("Time", "LLimisses"))
    column_pairs.append(("Time", "binsize"))
    column_pairs.append(("Time", "cache-misses"))

    column_pairs.append(("Time", "cache-references"))
    column_pairs.append(("Time", "dTLB-load-misses"))
    column_pairs.append(("Time", "dTLB-loads"))
    column_pairs.append(("Time", "dTLB-stores"))
    column_pairs.append(("Time", "dTLB_store_misses"))
    column_pairs.append(("Time", "latency"))
    column_pairs.append(("Time", "measured"))
    column_pairs.append(("Time", "mem-loads"))
    column_pairs.append(("Time", "mem-stores"))
    column_pairs.append(("Time", "size"))
    column_pairs.append(("Time", "sseavx"))

    for cx, cy in column_pairs:

        output_file = "{prefix}_{cx}_{cy}.png".format(prefix=prefix, cx=cx, cy=cy)

        cmd = "plot_wrapper.sh {input_file} {output_file} {x_column} {y_column}".format(
            input_file=icsv_file,
            output_file=output_file,
            x_column=cx,
            y_column=cy
            )

        print cmd

        os.system(cmd)

def process_all_folders():
    paths = []

    # paths.append("./../examples/strata/")
    # paths.append("./../examples/nacl_shootout/")

    paths.append("./../examples/p02/")
    paths.append("./../examples/p04/")
    paths.append("./../examples/p06/")
    paths.append("./../examples/p08/")
    paths.append("./../examples/p10/")
    paths.append("./../examples/p12/")
    paths.append("./../examples/p14/")
    paths.append("./../examples/p16/")
    paths.append("./../examples/p18/")
    paths.append("./../examples/p20/")
    paths.append("./../examples/p22/")
    paths.append("./../examples/p24/")
    paths.append("./../examples/parity/")

    paths.append("./../examples/exp/")
    paths.append("./../examples/montmul/")
    paths.append("./../examples/p01/")
    paths.append("./../examples/p03/")
    paths.append("./../examples/p05/")
    paths.append("./../examples/p07/")
    paths.append("./../examples/p09/")
    paths.append("./../examples/p11/")
    paths.append("./../examples/p13/")
    paths.append("./../examples/p15/")
    paths.append("./../examples/p17/")
    paths.append("./../examples/p19/")
    paths.append("./../examples/p21/")
    paths.append("./../examples/p23/")
    paths.append("./../examples/p25/")
    paths.append("./../examples/saxpy/")
    paths.append("./../examples/tutorial/")

    min_time_list = []
    max_time_list = []
    for p in paths:
        prefix = os.path.basename(os.path.normpath(p))
        print p, prefix

        out_csv = "%s.csv" % prefix
        in_txt = "%s/cost.txt" % p

        min_td, max_td = process_stoke_result_costs_file(in_txt, out_csv)
        min_td["Name"] = prefix
        max_td["Name"] = prefix

        min_time_list.append(min_td)
        max_time_list.append(max_td)

        # plot_relevant_columns(out_csv, prefix)


    for d in min_time_list:
        print d.keys()


    res_dic_2_csv(min_time_list, "min_indexes.csv", col_index = 10)
    res_dic_2_csv(max_time_list, "max_indexes.csv", col_index = 10)




if __name__ == '__main__':

    parser = argparse.ArgumentParser("""This script processes Stoke Costs result file and converts
        it to a readable CSV format""",
        add_help=True,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--costs_result_file', type=str,
        help="""Input costs result file (e.g., cost.txt)""")

    parser.add_argument('--out_csv', type=str, default="res.csv",
        help="""Output CSV file (e.g., res.csv)""")


    FLAGS = parser.parse_args()

    process_all_folders()


    # process_stoke_result_costs_file(FLAGS.costs_result_file, FLAGS.out_csv)

    # plot_relevant_columns(FLAGS.out_csv)



