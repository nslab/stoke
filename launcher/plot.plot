#!/usr/bin/gnuplot

set terminal png dl 1.5 enhanced  size 1000.0, 700.0 font "Helvetica,15"

set datafile separator ","

set ylabel 'Value ' offset 0,0
set xlabel 'Execution Time  [sec]' offset 0,0.8

unset xrange
set grid

set output OUT_PUT
# set title TITLE



plot \
  INPUT_FILE u X_COLUMN:Y_COLUMN with points ps 4