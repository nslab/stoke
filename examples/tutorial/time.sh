#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Wrong Input! Please run the script in this format:"
  echo "./$0 <Input program>"
  exit 0
fi

check_perf_cmd="cat /proc/sys/kernel/perf_event_paranoid"
check_perf_val=$(eval $check_perf_cmd)
if [ "$check_perf_val" != "-1" ]; then
  echo "Run this first:"
  echo "sudo sh -c 'echo -1 >/proc/sys/kernel/perf_event_paranoid'"
  exit 0
fi

input_file=$1
repeat_time=10
perf_cmd="perf stat -r $repeat_time -B ./$input_file > /dev/null 2>time.log"
eval $perf_cmd
#awk '$2=="seconds" {print $1}' time.log
time_awk_cmd="awk '"'$2 == "seconds"'" {print "'$1}'"' time.log"
time_measured=$(eval $time_awk_cmd)
echo $time_measured

