#/bin/bash

# Name
# A_sample_count
# Cycles
# D1misses
# I1misses
# L1-dcache-load-misses
# L1-dcache-loads
# L1-dcache-stores
# L1-icache-load-misses
# LLC-loads
# LLC_stores
# LLdmisses
# LLimisses
# Time
# binsize
# cache-misses
# cache-references
# dTLB-load-misses
# dTLB-loads
# dTLB-stores
# dTLB_store_misses
# latency
# measured
# mem-loads
# mem-stores
# size
# sseavx


SCRIPTS_DIR="./"


for COLUMN in Cycles D1misses I1misses L1-dcache-load-misses L1-dcache-loads L1-dcache-stores L1-icache-load-misses LLC-loads LLC_stores LLdmisses LLimisses Time binsize cache-misses cache-references dTLB-load-misses dTLB-loads dTLB-stores dTLB_store_misses latency measured mem-loads mem-stores size sseavx
do

gnuplot -e "X_COLUMN='Time'"  -e "Y_COLUMN='$COLUMN'"   -e "OUTPUT_FOLDER='./out_raw/$COLUMN'" ./plot_raw.plot

done

COLUMN=binsize
YLABEL="Binary size [bytes]"
gnuplot -e "X_COLUMN='Time'"  -e "Y_COLUMN='$COLUMN'"   -e "OUTPUT_FOLDER='./out_raw/$COLUMN'" -e "YLABEL='$YLABEL'" ./plot_raw.plot

COLUMN=latency
YLABEL="Latency [instructions]"
gnuplot -e "X_COLUMN='Time'"  -e "Y_COLUMN='$COLUMN'"   -e "OUTPUT_FOLDER='./out_raw/$COLUMN'" -e "YLABEL='$YLABEL'" ./plot_raw.plot