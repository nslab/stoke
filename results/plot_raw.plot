#!/usr/bin/gnuplot

set terminal png dl 1.5 enhanced  size 800.0, 600.0 font "Helvetica,15"

set datafile separator ","

if (!exists("YLABEL")) YLABEL='Value'

set ylabel YLABEL offset 0,0
set xlabel 'Execution Time  [sec]' offset 0,0.8

unset xrange
set grid

# X_COLUMN="Time"
# Y_COLUMN="I1misses"
# OUTPUT_FOLDER="./out/"
system("mkdir -p ".OUTPUT_FOLDER)

set output OUTPUT_FOLDER."/raw_saxpy.png"
INPUT_FILE="./data_raw/saxpy.csv"
plot \
  INPUT_FILE u X_COLUMN:Y_COLUMN with points ps 4

set output OUTPUT_FOLDER."/raw_p23.png"
INPUT_FILE="./data_raw/p23.csv"
plot \
  INPUT_FILE u X_COLUMN:Y_COLUMN with points ps 4

set output OUTPUT_FOLDER."/raw_p25.png"
INPUT_FILE="./data_raw/p25.csv"
plot \
  INPUT_FILE u X_COLUMN:Y_COLUMN with points ps 4

set output OUTPUT_FOLDER."/raw_p21.png"
INPUT_FILE="./data_raw/p21.csv"
plot \
  INPUT_FILE u X_COLUMN:Y_COLUMN with points ps 4

set output OUTPUT_FOLDER."/raw_p22.png"
INPUT_FILE="./data_raw/p22.csv"
plot \
  INPUT_FILE u X_COLUMN:Y_COLUMN with points ps 4

set output OUTPUT_FOLDER."/raw_p16.png"
INPUT_FILE="./data_raw/p16.csv"
plot \
  INPUT_FILE u X_COLUMN:Y_COLUMN with points ps 4