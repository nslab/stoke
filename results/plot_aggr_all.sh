#/bin/bash

### Columns
# Name
# A_sample_count
# Cycles
# D1misses
# I1misses
# L1-dcache-load-misses
# L1-dcache-loads
# L1-dcache-stores
# L1-icache-load-misses
# LLC-loads
# LLC_stores
# LLdmisses
# LLimisses
# Time
# binsize
# cache-misses
# cache-references
# dTLB-load-misses
# dTLB-loads
# dTLB-stores
# dTLB_store_misses
# latency
# measured
# mem-loads
# mem-stores
# size
# sseavx

# pdf or png
FORMAT=${1:-pdf}

if [[ -z $FORMAT ]]; then
	echo "Figure format should be either pdf or png"
	exit 1
fi

ALL_METRICS="Cycles D1misses I1misses L1-dcache-load-misses L1-dcache-loads L1-dcache-stores L1-icache-load-misses LLC-loads LLC_stores LLdmisses LLimisses Time binsize cache-misses cache-references dTLB-load-misses dTLB-loads dTLB-stores dTLB_store_misses latency measured mem-loads mem-stores size sseavx"
KEY_METRICS=(Cycles L1-dcache-load-misses LLdmisses cache-misses binsize size latency Time)
SELECTED_PROGRAMS=(p16 p21 p22 p23 p25 saxpy)
SELECTED_PROGRAM_INDICES=(10 28 13 29 30 31)
LEGEND_INDICES_X=(5.9 5.9 5.9 5.9 5.9 5.9)
LEGEND_INDICES_Y=(99 99 99 99 99 99)
ARRAY_LENGTH=${#SELECTED_PROGRAMS[@]}

for (( i=1; i<${ARRAY_LENGTH}+1; i++ ));
do
	APP=${SELECTED_PROGRAMS[$i-1]}
	APP_INDEX=${SELECTED_PROGRAM_INDICES[$i-1]}
	LEG_X_OFF=${LEGEND_INDICES_X[$i-1]}
	LEG_Y_OFF=${LEGEND_INDICES_Y[$i-1]}

	gnuplot \
		-e "TARGET_APP='$APP'" \
		-e "TARGET_APP_INDEX=$APP_INDEX" \
		-e "LEGEND_X_OFFSET=$LEG_X_OFF" \
		-e "LEGEND_Y_OFFSET=$LEG_Y_OFF" \
		-e "INPUT_FOLDER='./data_aggr'" \
		-e "OUTPUT_FOLDER='./out_aggr'" \
		-e "FORMAT='$FORMAT'" \
		./plot_aggr.plot
	echo ""
done

