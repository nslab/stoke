#!/usr/bin/gnuplot
load "./common.plot"

if (FORMAT eq 'png') {
	set terminal png dl 1.5 enhanced  size 1750.0, 750.0 font font_type
} else {
	set terminal pdf dl 1.5 enhanced dashed size 11.0, 5.5 font font_type
}

set style data histogram
set style histogram cluster gap 3
set datafile separator ","

### Variables
points_lw   = 1.0
points_size = 10
scale_ms = 1000000

# Input arguments
print "      Target App: ".TARGET_APP
print "Target App Index: ".TARGET_APP_INDEX
print " Legend X offset: ".LEGEND_X_OFFSET
print " Legend Y offset: ".LEGEND_Y_OFFSET
print "      In  Folder: ".INPUT_FOLDER
print "     Out  Folder: ".OUTPUT_FOLDER

# Input files with aggregate data
INPUT_FILE_MIN=INPUT_FOLDER."/min_indices_filtered.csv"
INPUT_FILE_MAX=INPUT_FOLDER."/max_indices_filtered.csv"

# Create the output folder if it does not already exist
system("mkdir -p ".OUTPUT_FOLDER)

### Y-axis
set ylabel "{/Helvetica:Bold Percentile (%)}"
set ylabel offset 2.0,0
set ylabel font font_type_bold
set ytics border in scale 1,0.5 norotate mirror
set ytics font font_type_bold
set yrange [0 : 101]

### X-axis
set xtics border in scale 1,0.5  norotate autojustify nomirror
set xtics font font_type_bold
set xtics 1
set xrange [0.5 : 6.5]

### Legend
set key inside
set key box linestyle 1 lt rgb(black)
set key at LEGEND_X_OFFSET, LEGEND_Y_OFFSET Right title
set key font font_type
set key samplen 2.0
set key width 0.0
set key height 0.7

### Title
set title TARGET_APP." application" offset 0,-0.7 font font_type_bold

### Margins
set tmargin at screen 0.945
set lmargin at screen 0.058
set rmargin at screen 0.995
set bmargin at screen 0.065

### Grid
set grid lw 1
set grid noxtics

### Linestyles
set style line 1 pointtype 4  pointsize points_size linewidth points_lw lt rgb(darkred)
set style line 2 pointtype 6  pointsize points_size linewidth points_lw lt rgb(black)
set style line 3 pointtype 8  pointsize points_size linewidth points_lw lt rgb(darkyellow)
set style line 4 pointtype 10 pointsize points_size linewidth points_lw lt rgb(darkgreen)
set style line 5 pointtype 12 pointsize points_size linewidth points_lw lt rgb(darkblue)
set style line 6 pointtype 14 pointsize points_size linewidth points_lw lt rgb(darkpurple)
set style line 7 pointtype 16 pointsize points_size linewidth points_lw lt rgb(black)
set style line 8 pointtype 18 pointsize points_size linewidth points_lw lt rgb(magenta)
set style line 9 pointtype 2  pointsize points_size linewidth points_lw lt rgb(darkpurple)

set output OUTPUT_FOLDER."/".TARGET_APP.".".FORMAT

plot \
	INPUT_FILE_MIN using TARGET_APP_INDEX:xtic(1) with hist title "Fastest" ls 1 fillstyle pattern 1,\
	INPUT_FILE_MAX using TARGET_APP_INDEX:xtic(1) with hist title "Slowest" ls 2 fillstyle pattern 4
