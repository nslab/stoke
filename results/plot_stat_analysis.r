#!/usr/bin/env Rscript

list.of.packages <- c("ggplot2", "plyr", "car", "GGally", "corrplot", "pls", "scales", "ggpubr", "rlang")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages, repos = "http://cran.us.r-project.org")

library(ggplot2)
library(ggpubr)
library(plyr)
library(car)
library(scales)


if(!exists("color_cor", mode="function")) source("utils.r")
if(!exists("moveme", mode="function")) source("utils.r")

plot_dir = "out_analysis"
plot_file_names = c("", "", "cor_matrix_basic_all.png", "", "linear_regression_1.png",
"", "cor_matrix_detailed.png", "linear_regression_2.png", "valgrind_versus_actual_cor.png",
"cost_function_convergence_comparison_all_apps.png", "cost_function_mape_all_apps.png")

plot_file_paths = c()
for(f in plot_file_names) {
	plot_file_paths = c(plot_file_paths, paste(plot_dir, f, sep="/"))
}

#--Task 1
print("[1] Reading data")
dir <- "./data_raw"
print(paste("Reading data from", dir, sep=" "))
names <- list.files(path = dir, pattern = "\\.csv$", all.files = TRUE, recursive = FALSE)

df <- data.frame()
for(n in names) {
	t <- read.csv(paste(dir, n, sep="/"), header=T)
	t$App <- strsplit(n, "[.]")[[1]][1]
	df <- rbind.fill(df, t)
}

#--Task 2
#remove unnecessary columns
print("[2] Cleaning up data")
df <- df[is.finite(rowSums(df[, unlist(lapply(df, is.numeric))])),] #no NA values
df <- df[, colSums(df != 0) > 0] #no all zero columns
columns <- colnames(df)
for(c1 in columns) { #no identical columns
	for(c2 in columns[seq(from=match(c1,columns), to=length(columns), by=1)]) {
		if(c1 == c2) next
		if(identical(df[[c1]],df[[c2]])) {
			df[[c1]] <- NULL #delete column
			break
		}	
	}
}
#move dependent variables to beginning of dataframe
df <- df[moveme(names(df), "Time first")]
df_per_app_list <- split(df, df$App) #produce per application datasets
df <- df[, unlist(lapply(df, is.numeric))] #no strings
for(i in 1:length(df_per_app_list)) {
	df_per_app_list[[i]] <- df_per_app_list[[i]][, unlist(lapply(df_per_app_list[[i]], is.numeric))] #no strings
}

#--Task 3
print("[3] Plotting correlation matrices for all predictors (for both aggregate and per app datasets)")
library("corrplot")
palette = "RdYlGn"
col <- colorRampPalette(RColorBrewer::brewer.pal(n = 5, name = palette))

cor.mtest <- function(mat, ...) {
    mat <- as.matrix(mat)
    n <- ncol(mat)
    p.mat<- matrix(NA, n, n)
    diag(p.mat) <- 0
    for (i in 1:(n - 1)) {
        for (j in (i + 1):n) {
            tmp <- cor.test(mat[, i], mat[, j], ...)
            p.mat[i, j] <- p.mat[j, i] <- tmp$p.value
        }
    }
  colnames(p.mat) <- rownames(p.mat) <- colnames(mat)
  p.mat
}

#plot aggregate correlation matrix
df_cor <- cor(df, method="pearson")
p_mat <- cor.mtest(df) # matrix of the p-value of the correlation
png(plot_file_paths[3], height = 600, width = 800)
corrplot(df_cor, method="color", col=col(200),  
         type="upper",
         addCoef.col = "black", # Add coefficient of correlation
         tl.col="black", tl.srt=45, #Text label color and rotation
	 number.cex= 10/ncol(df), #Text size for cor values
         # Combine with significance
         p.mat = p_mat, sig.level = 0.01, insig = "blank", 
         # hide correlation coefficient on the principal diagonal
         diag=FALSE 
         )
invisible(dev.off())
print(paste("Saved plot at:", plot_file_paths[3], sep=" "))

#plot per app correlation matrices
for(app_name in names(df_per_app_list)) {
	#skip to next iteration if nrows is too small
	if(nrow(df_per_app_list[[app_name]]) <= 1) next
	app.df_cor <- cor(df_per_app_list[[app_name]], method="pearson")
	app.plot_dir <- paste(plot_dir, app_name, sep="/")
	app.plot_file_path = paste(app.plot_dir, plot_file_names[3], sep="/")
        if (!dir.exists(app.plot_dir)) dir.create(app.plot_dir)
	#plot aggregate correlation matrix
	app.p_mat <- cor.mtest(df_per_app_list[[app_name]]) # matrix of the p-value of the correlation
	png(app.plot_file_path, height = 600, width = 800)
	corrplot(app.df_cor, method="color", col=col(200),  
		 type="upper",
		 addCoef.col = "black", # Add coefficient of correlation
		 tl.col="black", tl.srt=45, #Text label color and rotation
		 number.cex= 10/ncol(df), #Text size for cor values
		 # Combine with significance
		 p.mat = app.p_mat, sig.level = 0.01, insig = "blank", 
		 # hide correlation coefficient on the principal diagonal
		 diag=FALSE 
		 )
	invisible(dev.off())
	print(paste("Saved plot at:", app.plot_file_path, sep=" "))
}

#--Task 4
print("[4] Producing training and verification datasets..")
#remove redundant predictors to make cor matrix more readible
df_tiny <- df[, -which(names(df) %in% c("L1.dcache.load.misses", "L1.dcache.loads",
"L1.icache.load.misses", "L1.dcache.stores", "L1.icache.load.misses", "LLC.loads",
"LLC_stores", "dTLB.loads", "dTLB.stores", "cache.references", "cache.misses",
"mem.stores", "mem.loads"))]

#get training/testing sets (80:20 split)
set.seed(100)  # setting seed to reproduce results of random sampling
training_idx = sample(nrow(df),replace=F,size=0.8*nrow(df)) #80% hardcoded
training_set = df[training_idx,]
testing_set = df[-training_idx,]

#--Task 5
print("[5] Fitting model using linear regression & plotting results")
fit1<- lm(Time~D1misses+I1misses+LLdmisses+LLimisses+binsize+measured+size, data=training_set)
#fit1<- lm(Time~D1misses+I1misses+LLdmisses+LLimisses+binsize+measured+size+
#L1.dcache.loads+L1.dcache.stores+LLC.loads+
#LLC_stores+dTLB.load.misses+dTLB.loads+dTLB_store_misses+dTLB.stores, data=training_set)
print(fit1$coefficients)
png(plot_file_paths[5], height = 600, width = 800)
par(mfrow=c(2,2))
plot(fit1)
invisible(dev.off())
print(paste("Saved plot at:", plot_file_paths[5], sep=" "))

print("Validating model..")
fit1_results <- data.frame(cbind(actual=testing_set$Time, predicted=predict(fit1, testing_set)))

min_max_accuracy <- mean(apply(fit1_results, 1, min) / apply(fit1_results, 1, max))
print(paste("min-max accuracy =", min_max_accuracy, collapse=" "))
mape <- mean(abs((fit1_results$predicted - fit1_results$actual))/fit1_results$actual) * 100
print(paste("mean absolute percentage error =", mape, collapse=" "))

#--Task 6
print("[6] Computing VIF for first model")
fit1_vif = vif(fit1) #helps identify sources of colinearity
print(fit1_vif)

#--Task 7
print("[7] Plotting aggregate correlation matrix (only for candidate predictors)")

#make visualized data even more tiny
df_tiny <- df_tiny[, -which(names(df_tiny) %in% c("size"))]

#reorder things to improve appearance
df_tiny <- df_tiny[moveme(names(df_tiny), "binsize last")]

lowerFn <- function(data, mapping, method = "lm", ...) {
  p <- ggplot(data = data, mapping = mapping) +
    geom_point(colour = "black", alpha=0.2, size=1) +
    scale_y_continuous(breaks=round(range(data[,as.character(mapping$y)])), labels = scientific_format(digits=1)) +
    scale_x_continuous(breaks=round(range(data[,as.character(mapping$x)])), labels = scientific_format(digits=1)) +
    theme(axis.text.x=element_text(angle=90,hjust=1)) +
    geom_smooth(method = method, color = "blue", size=0.5, ...)
}

png(plot_file_paths[7], height = 600, width = 800)
library(GGally)
ggpairs(df_tiny, lower=list(continuous= wrap(lowerFn, method="lm")),
  diag=list(continuous= wrap("barDiag", colour="blue", bins=15)),
  upper=list(continuous= wrap(color_cor, method="pearson")))
invisible(dev.off())
print(paste("Saved plot at:", plot_file_paths[7], sep=" "))

#--Task 8
print("[8] Refitting model based on candidate predictors & plotting results")
fit2<- lm(Time~D1misses+I1misses+LLdmisses+LLimisses+dTLB.load.misses+dTLB_store_misses+binsize+measured, data=training_set)
print(fit2$coefficients)
#library(pls)
#fit2 <- pcr(Time~., data = training_set[, -which(names(training_set) %in% c("Cycles"))], scale = TRUE, validation = "CV")
png(plot_file_paths[8], height = 600, width = 800)
par(mfrow=c(2,2))
plot(fit2)
invisible(dev.off())
print(paste("Saved plot at:", plot_file_paths[8], sep=" "))

print("Validating model..")
fit2_results <- data.frame(cbind(actual=testing_set$Time, predicted=predict(fit2, testing_set)))

min_max_accuracy <- mean(apply(fit2_results, 1, min) / apply(fit2_results, 1, max))
print(paste("min-max accuracy =", min_max_accuracy, collapse=" "))
mape <- mean(abs((fit2_results$predicted - fit2_results$actual))/fit2_results$actual) * 100
print(paste("mean absolute percentage deviation =", mape, collapse=" "))

#--Task 9
print("[9] Comparing accuracy of valgrind and perf predictors")
x <- c("D1misses", "LLdmisses", "L1.dcache.load.misses", "cache.misses")
y <- c()

for(c in x) {
y <- c(y, df_cor[match(c, colnames(df)), match("Time", colnames(df))])
}

xy <- t(data.frame(y[1:2], y[3:4]))

png(plot_file_paths[9], height = 600, width = 800)
old.par <- par(mar = c(5,8,4,2)+0.1)
bp <- barplot(xy, beside = TRUE,
col = c("mistyrose", "lavender"),
xlab = "Event Type", names = c("D1 Miss", "LLd Miss"),
ylab = "Correlation with Total Execution Time", legend = c("Valgrind", "perf"),
args.legend = list(title = "Measurement", x = "topright", cex = .7), ylim=c(-0.5,1),
cex.axis=1.5, cex.names=1.5, cex.lab=1.5)
text(bp, 0, round(xy, 1),cex=1,pos=3)
par(old.par)
invisible(dev.off())
print(paste("Saved plot at:", plot_file_paths[9], sep=" "))

print("Use model to predict application datasets")

#--Task 10
print("[10] Plotting converged cost values of model compared to baselines for all application datasets")
fit2_app = data.frame(name=character(), mape=double(), cost=character(), time=character(), stringsAsFactors=FALSE)
for(app_name in names(df_per_app_list)) {
        if(app_name == "p16" || app_name == "p21" || app_name == "p22" || app_name == "p23" || app_name == "p25" || app_name == "saxpy") {
                app.df_tiny <- df_per_app_list[[app_name]]
                app.fit2_results <- data.frame(cbind(actual=app.df_tiny$Time, predicted=predict(fit2, app.df_tiny)))
                app.fit2_results$percentile = sapply(X = app.fit2_results$actual, function(x) ecdf(app.fit2_results$actual)(x))
                app.fit2_results$latency <- app.df_tiny$measured
                app.fit2_results$binsize <- app.df_tiny$binsize
		
		min_predicted_percentiles = app.fit2_results[which(app.fit2_results$predicted == min(app.fit2_results$predicted)),]$percentile
		min_latency_percentiles = app.fit2_results[which(app.fit2_results$latency == min(app.fit2_results$latency)),]$percentile
		min_binsize_percentiles = app.fit2_results[which(app.fit2_results$binsize == min(app.fit2_results$binsize)),]$percentile

		mape <- mean(abs((app.fit2_results$predicted - app.fit2_results$actual))/app.fit2_results$actual) * 100

                fit2_app = rbind(fit2_app, data.frame(name=app_name, mape=mape, cost="model", time=min_predicted_percentiles)) 
                fit2_app = rbind(fit2_app, data.frame(name=app_name, mape=mape, cost="latency", time=min_latency_percentiles))
                fit2_app = rbind(fit2_app, data.frame(name=app_name, mape=mape, cost="binsize", time=min_binsize_percentiles))

                print(paste("mean absolute percentage deviation for app :", app_name, "=", mape, collapse=""))
        }
}

png(plot_file_paths[10], height = 600, width = 800)
ggboxplot(fit2_app, x = "cost", y = "time",
          color = "cost", palette = "jco",
          add = "jitter", facet.by = "name",
	  short.panel.labs = FALSE, xlab = "Cost Function",
	  ylab = "Execution Time (percentile) - Lower is better") +
	  scale_x_discrete(labels = c("Model", expression(STOKE[latency]), expression(STOKE[binsize]))) +
	  theme(axis.text.x = element_text(margin = margin(t = 10)),
	  axis.title.x = element_text(margin = margin(t = 20), size=20), strip.text.x = element_text(size = 15),
	  axis.title.y = element_text(margin = margin(r = 20), size=20), legend.position="none")
invisible(dev.off())
print(paste("Saved plot at:", plot_file_paths[10], sep=" "))

#--Task 11
print("[11] Plotting MAPE values of model for all application datasets")
mapes = fit2_app[!duplicated(fit2_app$name),]

png(plot_file_paths[11], height = 600, width = 800)
old.par <- par(mar = c(5,8,4,2)+0.1)
bp <- barplot(mapes$mape, beside = TRUE,
col = c("mistyrose", "lavender"),
xlab = "Application", names = mapes$name,
ylab = "Mean Absolute Percentage Error (MAPE) (%)",
cex.axis=1.5, cex.names=1.5, cex.lab=1.5)
par(old.par)
invisible(dev.off())

print(paste("Saved plot at:", plot_file_paths[11], sep=" "))

